# node-streams

Test node streams

## Tools

| Tool   | Version |
| ------ | :-----: |
| Nodejs | v20.9.0 |

## Run server

```
yarn dev
```

```
pnpm run dev
```

```
npm run dev
```

Send data to server

```
node src/fake-upload-to-http-stream
```